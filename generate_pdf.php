
<?php
//include connection file 
include_once('fpdf181/fpdf.php');
include 'DBController.php';
 
class PDF extends FPDF
{
// Page header
function Header()
{
    $this->SetFont('Arial','B',13);
    // Move to the right
    $this->Cell(80);
    $this->Ln(20);
}
 
// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
} 
$db_handle = new DBController();
$connString=$db_handle->connectDB();
$display_heading = array('id'=>'ID', 'name'=> 'Product Name', 'price'=> 'Price','category'=> 'Category','average_rating'=>'Average Rating');
 
$result = $db_handle->runQuery("SELECT id, name, price, category,average_rating FROM tbl_products");
$header = mysqli_query($connString, "SHOW columns FROM tbl_products");
 
$pdf = new PDF();
//header
$pdf->AddPage();
//foter page
$pdf->AliasNbPages();
$pdf->SetFont('Arial','B',12);
foreach($header as $heading) {
$pdf->Cell(40,12,$display_heading[$heading['Field']],1);
}
foreach($result as $row) {
$pdf->Ln();
foreach($row as $column)
$pdf->Cell(40,12,$column,1);
}
$pdf->Output();
?>
